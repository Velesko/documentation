<?php
/**
 * @category Work in progress
 * @author Vlastislav Šíp
 * @category CMS
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version 0.0.0
 * 
 */
class prikazy {
    public $uzivatel; //Username from database
    public $heslo;  //Password from database
    public $table;  //Name of table, where you want to have a information
    public $dbname; //Name of your's database
    public $host;   //default is localhost or 127.0.0.1 - this is the way to the server.
/** Construct
 * 
 * This have to be there. Only make the variables empty.í,
 * 
 */
  public function __construct() {
      $this->uzivatel = "";
      $this->heslo = "";
      $this->table = "";
      $this->dbname = "";
  }
  /**   Parser
   * 
   *    Parser is function, when you send the variables, like a password and name from database, and the parser save this variables to local variables.
   * 
   * @param type $database
   * @param type $name
   * @param type $passw
   * @param type $host
   * @param type $table
   */
  public function parser($database, $name, $passw, $host, $table) {
      $this->uzivatel = $name;
      $this->heslo = $passw;
      $this->table = $table;
      $this->dbname = $database;
      $this->host = $host;
      
  }
  /**   Site
   *    This part of program is the main part of the program. This function is used everytime, when you are on the website. This function is for write the information from database to site.
   * @return type
   */
  public function uzivatel() {
    $r = mysql_query("SELECT * FROM $this->table ORDER BY datum DESC");
    
    if ( $r !== false && mysql_num_rows($r) > 0 ) {
      while ( $query = mysql_fetch_assoc($r) ) {
        $nadpis = $query['nadpis'];
        $obsah = $query['obsah'];

        $uzivatel .= <<<UZIVATEL

    <div class="vypis">
    	<h2>$nadpis</h2>
	    <p>
	      $obsah
	    </p>
	</div>

UZIVATEL;
      }
    } else {
      $uzivatel = <<<UZIVATEL

    <h1> Tato stránka je ve fázi vývoje </h1>
    <p>
      Ještě nebyl přidán žádný příspěvek, možná je na čase nějaký přidat?
    </p>

UZIVATEL;
    }
    $uzivatel .= <<<ADMIN

    <p class="admin_link">
      <a href="{$_SERVER['PHP_SELF']}?admin=1">Přidejte článek</a>
    </p>

ADMIN;

    return $uzivatel;
  }
/** Admin form
 * 
 * This part of program write an admin form into the website, if you are logged as administrator. In my program you only have to add '?admin=1' after the url address and hit enter.
 * 
 * @return type
 */
  public function admin() {
    return <<<ADMIN
      
      <form action="{$_SERVER['PHP_SELF']}" method="post">
        <label for="Nadpis">nadpis:</label>
        <input name="nadpis" id="nadpis" type="text" maxlength="150" />
        <label for="Hltext">Obsah:</label>
        <textarea name="hltext" id="hltext"></textarea>
        <input type="submit" value="Vytvořit" />
      </form>
      <a href="index.php">Zpět</a>
      
ADMIN;
  }
/** Write
 * 
 * This part of program is there for including new rows to the table in database.  
 * 
 * @return boolean
 */
  public function zapsat() {
    if ($_POST['nadpis']){
    $nadpis = $_POST['nadpis'];
    }else{
    echo "chyba - nebyl zadán nadpis";
    }if ($_POST['hltext']){
    $hltext = $_POST['hltext'];
    }else{
        echo "chyba-nebyl zadán nadpis";
    }
    if ($nadpis && $hltext) {
      $q = $conn->prepare("INSERT INTO $this->table (`nadpis`,`obsah`,`datum`) VALUES (?,?,?)");
    $q->bindParam(1, $nadpis);
    $q->bindParam(2, $hltext);
    $datum = StrFTime("%d/%m/%Y", Time());
    $q->bindParam(3, $datum);
    return $q->execute();
    } else {
      return false;
    }
  }
/** PDO Connection
 * This part of program have a simple quest, take a local variables and use it in connection to database.
 * 
 * @var $conn - this is variable where is saved the command for SQL
 * 
 */
  public function pdo_connect() {
      
    $conn = new PDO("mysql:host=$this->host;dbname=$this->dbname","$this->uzivatel","$this->heslo") or die ("Nemohl jsem se spojit se serverem" . mysql_error());
    $conn->query('SET NAMES UTF8');
    
      $sql = "CREATE TABLE IF NOT EXISTS $this->table(
            ID          int(12) AUTO_INCREMENT PRIMARY KEY,
            nadpis	VARCHAR(150),
            obsah	TEXT,
            datum	VARCHAR(100)
    )";
      
    $stm = $conn->prepare($sql);
    $stm->execute();
    
  }
}
